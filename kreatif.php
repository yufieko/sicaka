<?php 
  session_start();
  $_SESSION['current_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  include 'inc/koneksi.php';

  if($_SESSION['level']!='kreatif'){
    header("Location: login.php");
  }else{
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/Merah.png">
    <title>Sicaka</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="./css/style.css" rel="stylesheet">
    <link href="css/daterangepicker.css" rel="stylesheet">
    <link href="css/select2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  </head>
  <!-- <style type="text/css">
    .logo{
      display: block;
    }
    #images{
      display: none;
    }
  </style> -->
  <body style="background: #F4F7F6;">

    <nav class="navbar-default navbar-fixed-top" style="border-radius: 0px; background: #183544;">
      <div class="container" style="color: #fff;">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a style="color: #fff !important;" class="navbar-brand logo" href="kreatif.php"><img class="logo_judul" src="img/Putih.png"><p style="margin-top: -28px; margin-left: 48px;"><b style="font-size: 25pt;"> Sicaka </b></p></a>
          <button type="button" id="images" data-toggle="collapse" data-target="#nav-content" class="btn btn-primary indeks"><i class="glyphicon glyphicon-align-left"></i> <span class="hidden-xs hidden-sm">Filter</span> </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav" id="search">
            <form action="" method="post">
                <input class="cari" name="cari" type="text" size="40" placeholder="Search...">
            </form>
          </ul>

          <ul class="nav navbar-nav navbar-right">

            <li>
              <button id="upload" style="margin-top: 16px; height: 35px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><p style="margin-top: -1px;">Upload gambar <i class="glyphicon glyphicon-cloud-upload"></i> </p></button>
            </li>
            
            <?php 
              include 'inc/koneksi.php';

              // $user_check=$_SESSION['login_user'];

              $tampil_data = mysqli_query( $link,"SELECT * FROM tbl_login WHERE email ='".$_SESSION['login_user']."'")or die(mysqli_error($link));
              $data = mysqli_fetch_array($tampil_data);

            ?>
            <li class="dropdown">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <p id="Welcome">Welcome, <?php echo $data['nama_depan']; ?> <span class="glyphicon glyphicon-chevron-down"></span></p>
              </a>

              <ul class="dropdown-menu">
                <li><a id="user" href="#"><i class="glyphicon glyphicon-user"></i> <?php echo $data['nama_depan'];?> <?php echo $data['nama_belakang']; ?></a></li>
                <li><a id="user" href="#"><i class="glyphicon glyphicon-envelope"></i> <?php echo $data['email']; ?></a></li>
                <li class="divider"></li>
                <li><a id="user" href="logout.php?logout"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
              </ul>

            </li>

          </ul>
        </div>
      </div>
      <div class="collapse navbar-toggleable-lg mini_atas" id="nav-content" style="border-top: 1px solid #0E1F28;">
        <div class="container">
          <ul id="mini" class="nav navbar-nav">
            <form action="cari_kreatif.php" method="get" class="form-inline">
              <div class="form-group">
                <select id="select" name="jenis" class="form-control" required>
                  <option value="" disabled selected >Pilih Jenis Tipe</option>
                  <option value="infografis">Infografis</option>
                  <option value="video">Video</option>
                  <option value="minigram">Minigram</option>
                </select>
                <input type="text" id="daterange" class="form-control tanggal" name="daterange" value="YYYY-MM-DD" required>
                <input type="submit" class="btn btn-primary tanggal" id="terapkan" value="Terapkan" name="cari2">
              </div>
            </form>
          </ul>
        </div>
      </div>
    </nav>
    <!-- <nav id="mini" class="mini_bawah" style="background: #183544; margin-top: 64px; border-top: 1px solid #0E1F28;">
      <div class="container">
        <ul class="nav navbar-nav">
          <form action="cari_kreatif.php" method="get" class="form-inline">
            <div class="form-group">
              <select id="select" name="jenis" class="form-control" required>
                <option disabled selected >Pilih Jenis Tipe</option>
                <option value="infografis">Infografis</option>
                <option value="video">Video</option>
                <option value="minigram">Minigram</option>
              </select>
              <input type="text" id="daterange" class="form-control tanggal" name="daterange" value="YYYY-MM-DD" />
              <input type="submit" class="btn btn-primary tanggal" id="terapkan" value="Terapkan" name="cari2">
            </div>
          </form> 
        </ul>
      </div>
    </nav> -->
    
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
    
      <!-- Modal Content Upload image/video -->
        <form action="proses upload/proses_upload.php" method="POST" enctype="multipart/form-data">
            
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Upload gambar</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <img id="myImg" src="#" alt="your image" />
              </div>
              <div class="form-group">
                <select name="jenis" class="form-control" required>
                  <option value="" disabled selected >Pilih Jenis Tipe</option>
                  <option value="infografis">Infografis</option>
                  <option value="video">Video</option>
                  <option value="minigram">Minigram</option>
                </select>
              </div>
            <!--  <div class="form-group">
                <select name="folder" class="form-control" required>
                  <option value="" disabled selected >Pilih Folder</option>
                  <option value="video">Video</option>
                  <option value="sosial">Sosial</option>
                  <option value="gnfi">Gnfi</option>
                  <option value="teknologi">Teknologi</option>
                  <option value="opini">Opini</option>
                </select>
              </div> -->
              <div class="form-group">
                <select style="width: 100%;" name="tambah_folder" class="tokenizationSelect2 form-control" multiple="true" required>
                  <?php 
                    include 'inc/koneksi.php';

                    if ($data['folder']=="") {
                      $tambah_folder = $_POST['tambah_folder'];
                      $simpan = mysqli_query($link,"INSERT INTO upload VALUES('$tambah_folder')");
                      // $simpan = mysqli_query($sqlSimpan);
                    }

                      $tampil = mysqli_query($link, "SELECT DISTINCT folder FROM upload");
                      // $tampil = mysqli_query($sql) or die(mysqli_error($link));
                      while ($data = mysqli_fetch_array($tampil)) {

                    if (!empty($data['folder'])) {
                      ?> <option value="<?= $data['folder']; ?>"><?= $data['folder']; ?></option> <?php
                    }
                   } 
                  ?>
                </select>
              </div>
              <div class="form-group">
                <input type="text" name="link" class="form-control" placeholder="https://link-anda" required>
              </div>
            </div>
            <div class="modal-footer">
              <div class="form-group">
                <input type="file" class="filestyle" name="file" data-buttonBefore="true" accept=".jpg, .jpeg, .png" required onchange="ValidateSingleInput(this);">
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-danger" name="upload_file" value="upload">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div><br><br><br>


    <div class="container">
      <div class="row row1">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <form action="folder_kreatif.php" method="get">
                  <select id="kota" name="folder" class="form-control" onchange="this.form.submit()">
                    <option value=""></option>
                    <?php 
                      include 'inc/koneksi.php';

                      $tampil = mysqli_query($link, "SELECT DISTINCT folder FROM upload");
                      // $tampil = mysqli_query($sql) or die(mysqli_error($link));
                      while ($t = mysqli_fetch_array($tampil)) {
                      ?>
                      <option value="<?= $t['folder']; ?>"><?= $t['folder']; ?></option>
                    <?php  
                      }
                    ?>
                  </select>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br><br><br>
      <div style="margin-bottom: 30px"></div>
      <div class="row grid">
      <?php 
        include 'inc/koneksi.php';
        include 'pagination.php';

        $q = isset($_REQUEST['cari']) ? urldecode($_REQUEST['cari']) : ''; // untuk keyword pencarian
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1; // untuk nomor halaman
        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 3; // khusus style pagination 2 dan 3
        $rpp = 6; // jumlah record per halaman



        @$cari=$_POST['cari'];

        @$channel = $_GET['folder'];
        @$cari2 = $_GET['cari2'];
        @$jenis = $_GET['jenis'];
        @$daterange = $_GET['daterange'];
        $a = explode("-", $daterange);
        $tgl_awal = date("Y-m-d", strtotime($a[0]));
        @$tgl_akhir = date("Y-m-d", strtotime($a[1]));
        $tampil_data= mysqli_query($link, "SELECT u.nama, u.judul, u.u_facebook, u.u_web, u.u_twitter, u.u_instagram, u.tanggal, l.nama_depan, u.id_file, u.link FROM upload u JOIN tbl_login l ON id = user_id WHERE u.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir' AND u.jenis = '$jenis' AND u.nama LIKE '%$cari%' OR u.nama LIKE '%$cari%' ORDER BY id_file DESC");
        if(isset($_POST['cari'])){
          }

        // $tampil_data = mysqli_query($query1)or die(mysqli_error($link));
        $num = 1;
        $num2= 1;
        $cek = mysqli_num_rows($tampil_data);

        $tpages = isset($cek) ? ceil($cek / $rpp) : 1; // jumlah total halaman
        $count = 0; // untuk paginasi
        $i = ($page - 1) * $rpp; // batas paginasi
        $no_urut = ($page - 1) * $rpp; // nomor urut
        $reload = $_SERVER['PHP_SELF'] . "?cari=" . $q . "&amp;adjacents=" . $adjacents; // untuk link ke halaman lain
        //        pagination config end



        if ($cek > 0) {
        while (($count < $rpp) && ($i < $cek)) { 
            mysqli_data_seek($tampil_data, $i);
            $data = mysqli_fetch_array($tampil_data);

          ?>

          <div class="col-md-4 grid-item">
            <div class="thumbnail">
              <div class="header">
                <a href="details.php?id_file=<?php echo $data['id_file']; ?>">
                  <img class="img-cover" src="hasil_upload/<?php echo $data['nama']; ?>">
                </a>
              </div>
              <div class="caption" style="height: 190px">
                <b><?php echo $data['nama']; ?></b><br><br>
                <?php 
                  if ($data['u_web']=="") {
                    ?> <h5> Web = <i class="glyphicon glyphicon-remove remove"></i> </h5> <?php
                  }else{
                    ?> <h5> Web = <i class="glyphicon glyphicon-ok centang"></i> </h5> <?php
                  }
                  if ($data['u_facebook']=="") {
                    ?> <h5> Facebook = <i class="glyphicon glyphicon-remove remove"></i> </h5> <?php
                  }else{
                    ?> <h5> Facebook = <i class="glyphicon glyphicon-ok centang"></i> </h5> <?php
                  }
                  if ($data['u_twitter']=="") {
                    ?> <h5> Twitter = <i class="glyphicon glyphicon-remove remove"></i> </h5> <?php
                  }else{
                    ?> <h5> Twitter = <i class="glyphicon glyphicon-ok centang"></i> </h5> <?php
                  }
                  if ($data['u_instagram']=="") {
                    ?> <h5> Instagram = <i class="glyphicon glyphicon-remove remove"></i> </h5> <?php
                  }else{
                    ?> <h5> Instagram = <i class="glyphicon glyphicon-ok centang"></i> </h5> <?php
                  }
                ?>
              </div>
              <div class="footer" style="padding:2px">

                <div class="ket"> 
                  <?php echo $data['tanggal']; ?><br>
                  upload by : <?php echo $data['nama_depan']; ?>
                </div>

                <!-- Tombol Delete -->
                <p class="action button" data-placement="top" data-toggle="tooltip" title="Delete">
                  <button class="btn btn-danger btn-md" data-title="Delete" name="btn_delete" data-toggle="modal" data-target="#delete<?=$num2?>" >
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                </p>

                <!-- Tombol Edit -->
                <p class="action button3" data-placement="top" data-toggle="tooltip" title="Edit">
                  <button class="btn btn-primary btn-md" data-title="Edit" data-toggle="modal" data-whatever="@mdo" name="btn_edit" data-target="#edit<?=$num?>">
                    <span class="glyphicon glyphicon-pencil"></span>
                  </button>
                </p>

                <a href="details.php?id_file=<?php echo $data['id_file']; ?>" class="btn btn-info btn-md open_modal details1">Detail</a>
              </div>
            </div>
          </div>

          <!-- action modal edit -->
          <div class="modal fade" id="edit<?=$num?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  <h4 class="modal-title custom_align" id="Heading">Edit</h4>
                </div>
                

                <form action="proses-update-kreatif.php" method="post" enctype="multipart/form-data">

                  <div class="modal-body" id="modal_edit_kreatif">
                    <div class="form-group">
                     <img class="myImg1 image-preview" src="hasil_upload/<?php echo $data['nama']; ?>">
                    </div>
                    <div class="form-group">
                      <input type="hidden" name="id_file" value="<?php echo $data['id_file']; ?>">
                      <input type="file" class="filestyle image-source" accept=".jpg, .jpeg, .png" name="file" data-buttonBefore="true" onchange="previewImage('edit<?=$num++?>');ValidateSingleInput(this);"> 
                    </div>
                    <div class="form-group">
                      <label>&nbsp Edit Link</label>
                      <input class="form-control" type="text" name="link" value="<?= $data['link']; ?>">
                    </div>
                  </div>
                    
                  <div class="modal-footer">
                    <input type="submit" name="edit" value="Edit" class="btn btn-warning btn-lg" >
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- action modal delete -->
          <div class="modal fade" id="delete<?=$num2++?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                  <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">
                  <div class="alert alert-danger"><b><span class="glyphicon glyphicon-warning-sign"></span> Apakah Anda yakin ingin menghapus data ini ? </b></div>      
                </div>
                <div class="modal-footer ">
                  <a href="proses_delete.php?id_file=<?php echo $data['id_file']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> Ya
                  </a>
                  <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Tidak</button>
                </div>
              </div>
            </div>
          </div>
      <?php 
        $i++;
        $count++;
        }
      }else{
        ?>
          <div style="margin-bottom:20px; margin-top: 90px;"></div>
          <center><h1>Maaf, Data Yang Anda Cari Tidak Ada</h1></center> 
        <?php
      }
      ?>
      </div>
    </div><br><br>
    
       <center><?php echo paginate_one($reload, $page, $tpages, $adjacents); ?></center>

    <!-- <br><br><br>
    <nav id="footer" class="navbar navbar-default navbar-fixed-bottom">
      <div class="container-fluid">
    
      </div>
    </nav> -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/edit_profil.js"></script>
    <script src="js/auto _size.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/daterangepicker.js"></script>
    <script src="./js/jquery.form.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="https://unpkg.com/masonry-layout@4.1.1/dist/masonry.pkgd.min.js"></script>

    <script>
      $(document).ready(function () {
        $(".navbar-toggle").on("click", function () {
            $(this).toggleClass("active");
        });
    });
    </script>

    <script>
      $(document).ready(function(){
        $(".tokenizationSelect2").select2({
              placeholder: "Pilih Folder", //placeholder
              tags: true,
              maximumSelectionLength: 1,
              tokenSeparators: ['/',',',';'] 
          });
    //     $(".limitedNumbSelect2").select2({
    //       maximumSelectionLength: 1,
    //       placeholder: "Which are two of most productive days of your week"
    // })
      })
    </script>

    <!-- show image -->
    <script>
    function previewImage(modal) {
        var m = document.getElementById(modal);
        console.log(document.getElementById(modal).querySelector(".image-source").files[0])
        // m.find('#image-preview').show();
        // m.style.display = "block";
        var oFReader = new FileReader();
         oFReader.readAsDataURL(document.getElementById(modal).querySelector(".image-source").files[0]);
        oFReader.onload = function(oFREvent) {
          document.getElementById(modal).querySelector(".image-preview").src=  oFREvent.target.result;
        };
      };
    </script>
    <!-- -->
    
    <!-- untuk select -->
    <script>
      $(document).ready(function () {
        $("#kota").select2({
            placeholder: "Pilih Folder"
        });

        $("#kota2").select2({
            placeholder: "Please Select"
        });

        $('.grid').masonry({
          columnWidth: 390,
          itemSelector: '.grid-item'
        });
      });
    </script>

    <script>
      var _validFileExtensions = [".jpg", ".jpeg", ".png"];    
      function ValidateSingleInput(oInput) {
          if (oInput.type == "file") {
              var sFileName = oInput.value;
               if (sFileName.length > 0) {
                  var blnValid = false;
                  for (var j = 0; j < _validFileExtensions.length; j++) {
                      var sCurExtension = _validFileExtensions[j];
                      if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                          blnValid = true;
                          break;
                      }
                  }
                   
                  if (!blnValid) {
                      alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                      oInput.value = "";
                      return false;
                  }
              }
          }
          return true;
      }
    </script>

    <!-- Daterange Picker -->
    <script type="text/javascript">
      $(function() {
        $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        "minDate": "01/01/2015"
        }, function(start, end, label) {
            alert("New date range selected: " + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));
        });
      });
    </script>
    <!-- Selesai -->



    <script>
      $(document).ready(function(){
        $("#mytable #checkall").click(function () {
          if ($("#mytable #checkall").is(':checked')) {
              $("#mytable input[type=checkbox]").each(function () {
                  $(this).prop("checked", true);
              });

          } else {
              $("#mytable input[type=checkbox]").each(function () {
                  $(this).prop("checked", false);
              });
          } 
        });
      });
    </script>

    <!-- ganti profile -->
    <script>
      $(document).on('change', '#image_upload_file', function () {
        var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

      $('#image_upload_form').ajaxForm({
        beforeSend: function() {
        progressBar.fadeIn();
          var percentVal = '0%';
          bar.width(percentVal)
          percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
          var percentVal = percentComplete + '%';
          bar.width(percentVal)
          percent.html(percentVal);
        },
        success: function(html, statusText, xhr, $form) {   
          obj = $.parseJSON(html);  
            if(obj.status){   
              var percentVal = '100%';
              bar.width(percentVal)
              percent.html(percentVal);
              $("#imgArea>img").prop('src',obj.image_medium);     
                }else{
                    alert(obj.error);
                  }
                },
                complete: function(xhr) {
                  progressBar.fadeOut();      
                } 
              }).submit();    

              });
    </script>
    <!-- end ganti profil -->


    <!-- js untuk search gambar -->
    <script type="text/javascript">
      $(function () {
        $(":file").change(function () {
          if (this.files && this.files[0]) {
              var reader = new FileReader();
              reader.onload = imageIsLoaded;
              reader.readAsDataURL(this.files[0]);
            }
        });
      });

    function imageIsLoaded(e) {
        $('#myImg').attr('src', e.target.result);
      };
    </script>
    <!-- end js untuk search gambar -->

     <!-- js untuk search gambar -->
    <!-- <script type="text/javascript"> 
    //   $(function () {
    //     $(":file").change(function () {
    //       if (this.files && this.files[0]) {
    //           var reader = new FileReader();
    //           reader.onload = imageIsLoaded;
    //           reader.readAsDataURL(this.files[0]);
    //         }
    //     });
    //   });

    // function imageIsLoaded(e) {
    //     $('#myImg1').attr('src', e.target.result);
    //   };
    // </script>-->
     <!-- end js untuk search gambar -->

    <script>
        $(function(){
        $('.normal').autosize();
        $('.animated').autosize({append: "\n"});
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $( window ).scroll(function() {
          if($(window).scrollTop() > 190){
              $('').hide();
              $('#images').show();
          }else{
             $('.logo').show();
              $('').hide();
         }
        });
      });
    </script>
    <script>
      // document.getElementById("uploadBtn").onchange = function () {
      // document.getElementById("uploadFile").value = this.value;
      // };
    </script>
    
  </body>
</html>
<?php } ?>