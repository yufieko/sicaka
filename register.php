<?php 
  session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Registrasi</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/register.css" rel="stylesheet">
  </head>
  <body>
    <div class="col-md-4 col-md-offset-4 form-login">
        <div class="outter-form-login">
            <div class="logo-login">
                <em class="glyphicon glyphicon-user"></em>
            </div>
            <form action="" class="inner-login" method="post">
                <h3 class="text-center title-login"><b>Registrasi</b></h3>
                <div class="form-group">
                    <input type="hidden" name="id">
                    <input style="width:45%;" type="text" class="form-control" name="nama_depan" placeholder="Nama Depan"><input style="width:50%; float: right; margin-top: -34px;" type="text" class="form-control" name="nama_belakang" placeholder="Nama Belakang">
                </div>
                
                <div class="form-group">
                    <input type="date" class="form-control" name="tanggal">
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>

                <div class="form-group">
                    <input type="password" minlength="8" class="form-control" name="password" placeholder="Password">
                </div>

                <div class="form-group">
                    <select class="form-control" name="level">
                        <option disabled selected >Pilih Type</option>
                        <option value="kreatif">Kreatif</option>
                        <option value="redaksi">Redaksi</option>
                        <option value="user">User</option>
                    </select>
                </div>

                <input type="submit" name="register" class="btn btn-block btn-custom-green" value="REGISTER" />
                
                <div class="text-center forget">
                    <p>Kembali ke <a href="login.php"><b>Login</b></a></p>
                </div>
            </form>
            <?php 
                include 'inc/koneksi.php';
                if (isset($_POST['register'])) {
                    // echo "tombol sudah di klik";
                    $id = $_POST['id'];
                    $nama_depan = $_POST['nama_depan'];
                    $nama_belakang = $_POST['nama_belakang'];
                    $tanggal = $_POST['tanggal'];
                    $email = $_POST['email'];
                    $password = md5($_POST['password']);
                    $level = $_POST['level'];
                    $cek = mysqli_num_rows(mysqli_query($link,"SELECT * FROM tbl_login WHERE email='$email'"));

                    if ($cek > 0) {
                        ?> <script type="text/javascript">alert("email sudah ada yang pakai silahkan ganti dengan email lain");</script> <?php
                    }else{
                        if ($nama_depan == "" || $nama_belakang == "" || $tanggal == "" || $email == "" || $password == "" || $level == "") {
                            ?><script type="text/javascript">alert("inputan harus terisi semua");</script><?php
                        }else{
                            $sql = mysqli_query($link,"INSERT INTO tbl_login VALUES('$id','$nama_depan','$nama_belakang','$tanggal','$email','$password','$level')") or die (mysqli_error($link));
                            if ($sql) {
                                ?><script type="text/javascript">alert("Register anda berhasil, slahkan login");</script><?php
                            }
                        }
                    }
                }
             ?>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>