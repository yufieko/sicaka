<?php 

session_start(); //digunakan untuk memulai session

if (isset($_GET['logout'])) {
	unset($_SESSION['login_user']);
	session_unset();
	session_destroy();
	header("Location: login.php");
}
?>