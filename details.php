<?php 
  session_start();
  include 'inc/koneksi.php';

  if (empty($_SESSION['level'])) {
    header('location: login.php');
  }else{
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/Merah.png">
    <title>Sicaka</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link href="./css/style.css" rel="stylesheet">
    <link href="css/daterangepicker.css" rel="stylesheet">
    <link href="css/select2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  </head>

  <!-- <style type="text/css">
    .logo{
      display: block;
    }
    #images{
      display: none;
    }
  </style> -->

  <body style="background: #F4F7F6;">

    <nav class="navbar-default navbar-fixed-top" style="border-radius: 0px; background: #183544;">
      <div class="container" style="color: #fff;">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php
            if($_SESSION['level']=='user'){
              ?> 
              <a style="color: #fff !important;" class="navbar-brand logo" href="index.php"><img class="logo_judul" src="img/Putih.png"><p style="margin-top: -28px; margin-left: 48px;"><b style="font-size: 25pt;"> Sicaka </b></p></a> 
              <button type="button" id="images" data-toggle="collapse" data-target="#nav-content" class="btn btn-primary indeks"><i class="glyphicon glyphicon-align-left"></i> <span class="hidden-xs hidden-sm">Filter</span> </button>
              <?php
            }
            if($_SESSION['level']=='kreatif'){
              ?>
               <a style="color: #fff !important;" class="navbar-brand logo" href="kreatif.php"><img class="logo_judul" src="img/Putih.png"><p style="margin-top: -28px; margin-left: 48px;"><b style="font-size: 25pt;"> Sicaka </b></p></a> 
               <button type="button" id="images" data-toggle="collapse" data-target="#nav-content" class="btn btn-primary indeks"><i class="glyphicon glyphicon-align-left"></i> <span class="hidden-xs hidden-sm">Filter</span> </button>
              <?php
            }
            if($_SESSION['level']=='redaksi'){
              ?> 
              <a style="color: #fff !important;" class="navbar-brand logo" href="redaksi.php"><img class="logo_judul" src="img/Putih.png"><p style="margin-top: -28px; margin-left: 48px;"><b style="font-size: 25pt;"> Sicaka </b></p></a> 
              <button type="button" id="images" data-toggle="collapse" data-target="#nav-content" class="btn btn-primary indeks"><i class="glyphicon glyphicon-align-left"></i> <span class="hidden-xs hidden-sm">Filter</span> </button>
              <?php
            }
           ?>
          
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav" id="search">
            <form action="" method="post">
              <input class="cari" name="cari" type="text" size="40" placeholder="Search...">
            </form>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            
            <?php 
              include 'inc/koneksi.php';

              // $user_check=$_SESSION['login_user'];

              $tampil_data = mysqli_query($link,"SELECT * FROM tbl_login WHERE email ='".$_SESSION['login_user']."'")or die(mysqli_error($link));
              $data = mysqli_fetch_array($tampil_data);

            ?>
            <li class="dropdown">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <p id="Welcome">Welcome, <?php echo $data['nama_depan']; ?> <span class="glyphicon glyphicon-chevron-down"></span></p>
              </a>

              <ul class="dropdown-menu">
                <li><a id="user" href="#"><i class="glyphicon glyphicon-user"></i> <?php echo $data['nama_depan'];?> <?php echo $data['nama_belakang']; ?></a></li>
                <li><a id="user" href="#"><i class="glyphicon glyphicon-envelope"></i> <?php echo $data['email']; ?></a></li>
                <li class="divider"></li>
                <li><a id="user" href="logout.php?logout"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
              </ul>

            </li>

          </ul>
        </div>
      </div>
      <div class="collapse navbar-toggleable-lg mini_atas" id="nav-content" style="border-top: 1px solid #0E1F28;">
        <div class="container">
          <ul id="mini" class="nav navbar-nav">
            <?php
            if($_SESSION['level']=='user'){
              ?>
                <form action="cari.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option value="" disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary" id="terapkan" value="Terapkan" name="cari2">
                  </div>
                </form> 
              <?php
            }
            if($_SESSION['level']=='kreatif'){
              ?>
                <form action="cari_kreatif.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option value="" disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary" id="terapkan" value="Terapkan" name="cari2">
                  </div>
                </form>
              <?php
            }
            if($_SESSION['level']=='redaksi'){
              ?>
                <form action="cari_redaksi.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option value="" disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary" value="Terapkan" name="cari2">
                  </div>
                </form> 
              <?php
            }
           ?>
          </ul>
        </div>
      </div>
    </nav>
    <!-- <nav id="mini" class="mini_bawah" style="background: #183544; margin-top: 64px; border-top: 1px solid #0E1F28;">
      <div class="container">
        <ul class="nav navbar-nav">
          <?php
            if($_SESSION['level']=='user'){
              ?>
                <form action="cari.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control tanggal" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary tanggal" value="Terapkan" name="cari2">
                  </div>
                </form> 
              <?php
            }
            if($_SESSION['level']=='kreatif'){
              ?>
                <form action="cari_kreatif.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control tanggal" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary tanggal" value="Terapkan" name="cari2">
                  </div>
                </form>
              <?php
            }
            if($_SESSION['level']=='redaksi'){
              ?>
                <form action="cari_redaksi.php" method="get" class="form-inline">
                  <div class="form-group">
                    <select style="cursor: pointer;" id="select" name="jenis" class="form-control" required>
                      <option disabled selected >Pilih Jenis Tipe</option>
                      <option value="infografis">Infografis</option>
                      <option value="video">Video</option>
                      <option value="minigram">Minigram</option>
                    </select>
                    <input type="text" id="daterange" class="form-control tanggal" name="daterange" value="01/01/2017 - 01/31/2017" />
                    <input type="submit" class="btn btn-primary tanggal" value="Terapkan" name="cari2">
                  </div>
                </form> 
              <?php
            }
           ?>
        </ul>
      </div>
    </nav> -->
    
    <br><br><br><br><br><br>


    <div class="container">
      <div class="row grid">
      <?php 
        include 'inc/koneksi.php';

        @$cari=$_POST['cari'];
        $id_file = @$_GET['id_file'];

        $tampil_data = mysqli_query($link,"SELECT nama, judul, u_facebook, u_web, u_twitter, u_instagram, tanggal, nama_depan, id_file, link, jenis FROM upload JOIN tbl_login ON id = user_id WHERE nama LIKE '%$cari%' AND id_file='$id_file'");
        if(isset($_POST['cari'])){
          }

        // $tampil_data = mysqli_query($query1)or die(mysqli_error($link));
        $cek = mysqli_num_rows($tampil_data);

        if ($cek > 0) {
        while ($data = mysqli_fetch_array($tampil_data)) { 
          ?>

          <div class="col-md-8 col-md-offset-2 grid-item">
            <div class="thumbnail">
              <img class="img-cover" src="hasil_upload/<?php echo $data['nama']; ?>">
              <div class="caption">
                <b><?php echo $data['nama']; ?></b><br>
                <?php 
                  if ($data['judul']=="") {
                    ?> <p style="font-size: 13pt;"> judul belum di isi </p> <?php
                  }else{
                    ?> <p style="font-size: 13pt;"><?php echo $data['judul']; ?></p> <?php
                  }echo "<br>";
                ?>
                <?php 
                  if ($data['u_web']=="") {
                    
                  }else{
                    ?> 
                    <p><img  id="icon_img" src="img/gnfi.png"> Web </p>
                    <p><?php echo $data['u_web'];?></p> 
                    <hr>
                    <?php
                  }

                  if ($data['u_facebook']=="") {
                    
                  }else{
                    ?> 
                      <p><img  id="icon_img" src="img/fb.png"> Facebook </p>
                      <p><?php echo $data['u_facebook'];?></p>
                      <hr> 
                    <?php
                  }

                  if ($data['u_twitter']=="") {
                   
                  }else{
                    ?>
                      <p><img  id="icon_img" src="img/twitter.png"> Twitter </p>
                      <p><?php echo $data['u_twitter'];?></p>
                      <hr> 
                    <?php
                  }

                  if ($data['u_instagram']=="") {
                   
                  }else{
                    ?>
                      <p><img  id="icon_img" src="img/instagram.png"> Instagram </p>
                      <p><?php echo $data['u_instagram'];?></p>
                      <hr> 
                    <?php
                  }
                ?>
              </div>
              <div class="footer" style="height: auto;">
              
                <p class="caption" id="penjelasan"> 
                  jika ingin lebih jelas : <span class="label label-danger"><a style="color: #fff;" href="<?= $data['link']; ?>"><?php echo $data['link']; ?></a></span><br>
                  jenis yang upload : <span style="color: #fff;" class="label label-info"><?= $data['jenis']; ?></span><br>
                  tanggal upload : <span style="color: #fff;" class="label label-success"><?php echo $data['tanggal']; ?></span><br>
                  upload by : <span style="color: #fff;" class="label label-warning"><?php echo $data['nama_depan']; ?></span>
                </p>  

              </div>
            </div>
          </div>
          
      <?php
        }
      }else{
        echo "<center><h1>Maaf, Data Yang Anda Cari Tidak Ada</h1></center>";
      }
      ?>
      </div>
    </div>

    <br><br><br>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/edit_profil.js"></script>
    <script src="js/auto _size.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/daterangepicker.js"></script>
    <script src="./js/jquery.form.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="https://unpkg.com/masonry-layout@4.1.1/dist/masonry.pkgd.min.js"></script>

    <script>
      $(document).ready(function () {
        $(".navbar-toggle").on("click", function () {
            $(this).toggleClass("active");
        });
    });
    </script>

    <!-- untuk select -->
    <script>
      $(document).ready(function () {
        $("#kota").select2({
            placeholder: "Pilih Folder"
        });

        $("#kota2").select2({
            placeholder: "Please Select"
        });

        $('.grid').masonry({
          columnWidth: 390,
          itemSelector: '.grid-item'
        });
      });
    </script>


    <!-- Daterange Picker -->
    <script type="text/javascript">
      $(function() {
        $('input[name="daterange"]').daterangepicker({
        "autoApply": true,
        "minDate": "01/01/2015"
        }, function(start, end, label) {
            alert("New date range selected: " + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));
        });
      });
    </script>
    <!-- Selesai -->



    <script>
      $(document).ready(function(){
        $("#mytable #checkall").click(function () {
          if ($("#mytable #checkall").is(':checked')) {
              $("#mytable input[type=checkbox]").each(function () {
                  $(this).prop("checked", true);
              });

          } else {
              $("#mytable input[type=checkbox]").each(function () {
                  $(this).prop("checked", false);
              });
          } 
        });
      });
    </script>

    <!-- ganti profile -->
    <script>
      $(document).on('change', '#image_upload_file', function () {
        var progressBar = $('.progressBar'), bar = $('.progressBar .bar'), percent = $('.progressBar .percent');

      $('#image_upload_form').ajaxForm({
        beforeSend: function() {
        progressBar.fadeIn();
          var percentVal = '0%';
          bar.width(percentVal)
          percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
          var percentVal = percentComplete + '%';
          bar.width(percentVal)
          percent.html(percentVal);
        },
        success: function(html, statusText, xhr, $form) {   
          obj = $.parseJSON(html);  
            if(obj.status){   
              var percentVal = '100%';
              bar.width(percentVal)
              percent.html(percentVal);
              $("#imgArea>img").prop('src',obj.image_medium);     
                }else{
                    alert(obj.error);
                  }
                },
                complete: function(xhr) {
                  progressBar.fadeOut();      
                } 
              }).submit();    

              });
    </script>
    <!-- end ganti profil -->


    <!-- js untuk search gambar -->
    <script type="text/javascript">
      $(function () {
        $(":file").change(function () {
          if (this.files && this.files[0]) {
              var reader = new FileReader();
              reader.onload = imageIsLoaded;
              reader.readAsDataURL(this.files[0]);
            }
        });
      });

    function imageIsLoaded(e) {
        $('#myImg').attr('src', e.target.result);
      };
    </script>
    <!-- end js untuk search gambar -->

    <script>
        $(function(){
        $('.normal').autosize();
        $('.animated').autosize({append: "\n"});
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $( window ).scroll(function() {
          if($(window).scrollTop() > 190){
              $('').hide();
              $('#images').show();
          }else{
             $('.logo').show();
              $('').hide();
         }
        });
      });
    </script>
    <script>
      // document.getElementById("uploadBtn").onchange = function () {
      // document.getElementById("uploadFile").value = this.value;
      // };
    </script>
    
  </body>
</html>
<?php } ?>