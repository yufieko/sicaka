<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/Merah.png">
    <title>Halaman Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/register.css" rel="stylesheet">
  </head>
  <body>
    <div class="col-md-4 col-md-offset-4 form-login">
        <div class="outter-form-login">
            <div class="logo-login">
                <em class="glyphicon glyphicon-user"></em>
            </div>
            <form action="" class="inner-login" method="post">
            <h3 class="text-center title-login"><b>Login</b></h3>

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>

                <div class="form-group">
                    <input type="password" minlength="8" class="form-control" name="password" placeholder="Password">
                </div>

                <input type="submit" name="login" class="btn btn-block btn-custom-green" value="Login" />
                
                <div class="text-center forget">
                </div>
            </form>
            <?php 
            session_start();
              if (isset($_POST['login'])) {
                  // echo "tombol sudah di klik";
                  include 'inc/koneksi.php';
                  $email = $_POST['email'];
                  $password = md5($_POST['password']);

                  if ($email == "" || $password == "") {
                      ?><script type="text/javascript">alert("email dan password harus terisi");</script><?php
                    }else{
                        $sql = mysqli_query($link,"SELECT * FROM tbl_login WHERE email = '$email' and password = '$password'");
                        // $sql = mysqli_query($query) or die (mysqli_error($link));
                        $data = mysqli_fetch_array($sql);
                        $cek = mysqli_num_rows($sql);
                        $level = $data['level'];

                        if ($cek > 0) {
                          if ($level == "kreatif") {
                            $_SESSION['login_user'] = $email;
                            $_SESSION['level'] = $level;
                            $_SESSION['user_id'] = $data['id'];
                            header('location: kreatif.php');
                          }
                          elseif ($level == "redaksi") {
                            $_SESSION['login_user'] = $email;
                            $_SESSION['level'] = $level;
                            $_SESSION['user_id'] = $data['id'];
                            header('location: redaksi.php');
                          }
                          elseif ($level == "user") {
                            $_SESSION['login_user'] = $email;
                            $_SESSION['level'] = $level;
                            $_SESSION['user_id'] = $data['id'];
                            header('location:index.php');
                          }
                        }
                        else{
                            ?><script type="text/javascript">alert("Login gagal, username / password / type salah. Silahkan coba lagi")</script><?php
                        }
                    }
              }else{
                // echo "tombol belum di klik";
              }
             ?>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>